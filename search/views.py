from django.shortcuts import render
from mk_searchengine import search_f


def searching(request):

    if request.method == "POST":
        if request.POST['term']:
            return render(request, 'index.html', {
                    'result': search_f.nash_f(request.POST['term'], 10),
                    'zbor': request.POST['term']
            })
    return render(request, 'index.html', {})