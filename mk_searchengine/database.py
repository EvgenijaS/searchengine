import mysql.connector
import random


class Database(object):

    def __init__(self):
        self.db = mysql.connector.connect(host='localhost', user='root', passwd='sonne', db='django-final')
        self.curr = self.db.cursor()
        self.ins_crawled = "insert into crawled (url) values (%s)"
        self.ins_errors = "insert into errors (url, depth) values (%s, %s)"
        self.ins_links = "insert into links (from_url, to_url) values (%s,%s)"
        self.ins_tocrawl = "insert into tocrawl (url, depth) values (%s, %s)"
        self.ins_tocrawl_next_iteration = "insert into tocrawl_next_iteration (url, depth) values (%s, %s)"
        self.ins_words = "insert into words (word, url, freq) values (%s, %s, %s)"
        self.ins_test_words = "insert into test_words (url, words) values (%s, %s)"
        self.ins_pagerank = "insert into pagerank (url, rank) values (%s, %s)"
        self.ins_inverted = "insert into index_word (word, urls) values (%s, %s)"
        self.ins_ti = "insert into tfidf (url, tfidf) values (%s, %s)"
        self.ins_idf = "insert into idf (word, idf) values (%s, %s)"
        self.ins_pvk = "insert into puknati_vo_krolanje (url) values (%s)"
        self.ins_stop = "insert into stopwords(word) VALUES (%s)"

        self.del_tocrawl_query = "delete from tocrawl where url=%s"

    def get_link_tocrawl(self):
        try:
            self.db.commit()
            self.curr.execute('SELECT * FROM tocrawl')
            urls = self.curr.fetchall()
            if urls:
                idx = random.randint(0, len(urls)-1)

                self.insert_into_database('self.ins_crawled', (urls[idx][0],))
                self.del_tocrawl(urls[idx][0])

                #print urls[idx]
                return urls[idx]
            else:
                return None
        except Exception as e:
            print e.__str__() + ' from get_link_tocrawl()'
            return None

    def is_in_crawled(self, page):
        try:
            self.curr.execute("SELECT url FROM crawled WHERE url=%s", (page,))
            if self.curr.fetchone():
                return True
            return False
        except Exception as e:
            print e.__str__() + ' from is_in_crawled()'
            return False

    def is_in_tocrawl(self, page):
        try:
            self.curr.execute("SELECT url FROM tocrawl WHERE url=%s", (page,))
            if self.curr.fetchone():
                return True
            return False
        except Exception as e:
            print e.__str__() + ' from is_in_tocrawl()'
            return False

    def is_in_tocrawl_next_iteration(self, page):
        try:
            self.curr.execute("SELECT url FROM tocrawl_next_iteration WHERE url=%s", (page,))
            if self.curr.fetchone():
                return True
            return False
        except Exception as e:
            print e.__str__() + ' from is_in_tocrawl_next_iteration()'
            return False

    def is_in(self, page):
        if self.is_in_crawled(page) or self.is_in_tocrawl(page) or self.is_in_tocrawl_next_iteration(page):
            return True
        return False

    def del_tocrawl(self, page):
        self.curr.execute(self.del_tocrawl_query, (page,))
        self.db.commit()

    def __del__(self):
        self.db.commit()
        self.curr.close()
        self.db.close()

    def insert_into_database(self, query_name, data):
        try:
            self.curr.execute(eval(query_name), data)
            self.db.commit()
            return True
        except Exception as e:
            print e.__str__() + data[0]
            return False

    def insert(self, from_link, to_link, depth):
        self.insert_into_database('self.ins_links', (from_link, to_link,)) #glupost nevidena
        if not self.is_in(to_link):
            if depth < 7:
                self.insert_into_database('self.ins_tocrawl', (to_link, depth,))
            else:
                self.insert_into_database('self.ins_tocrawl_next_iteration', (to_link, depth,))

    def count_crawled(self):
        try:
            q = "select count(*) from crawled"
            self.curr.execute(q)
            data = self.curr.fetchone()
            return data[0]
        except Exception as e:
            return 0

    def count_tocrawl(self):
        try:
            q = "select count(*) from tocrawl"
            self.curr.execute(q)
            data = self.curr.fetchone()
            return data[0]
        except Exception as e:
            return 0

    def makeGraph(self):
        graph = {}
        q = "select * from links"
        self.curr.execute(q)
        data = self.curr.fetchall()

        for row in data:
            graph.setdefault(row[0], [])
            graph[row[0]].append(row[1])

        q = "select * from crawled"
        self.curr.execute(q)
        data = self.curr.fetchall()
        for row in data:
            graph.setdefault(row[0], [])
        f = open('C:\\Users\\Evgenija\\Desktop\\graph', 'w')
        f.write(graph.__str__())
        f.close()

    def takeWords(self):
        words = set([])
        q = "select * from test_words"
        self.curr.execute(q)
        data = self.curr.fetchall()
        for row in data:
            tmp = {}
            exec 'tmp = ' + row[1]
            for key in tmp:
                words.add(key)

        f = open('C:\\Users\\Evgenija\\Desktop\\words.txt', 'w')
        f.write(words.__str__())
        f.close()

    def getDocuments(self):
        q = "select * from test_words"
        self.curr.execute(q)
        data = self.curr.fetchall()
        documents = {}
        tmp = {}
        for row in data:
            exec 'documents[' + "'" + row[0] + "']=" + row[1]
        return documents

    def getDocumentsIDX(self):
        q = "select * from inv_tmp"
        self.curr.execute(q)
        data = self.curr.fetchall()
        return data

    #**************************************************
    def get_page_rank(self, url):
        try:
            q = "select pagerank.rank from pagerank where pagerank.url = \'%s\'" % url
            self.curr.execute(q)
            data = self.curr.fetchall()
            return float(data[0][0])
        except Exception as e:
            print e.__str__() + " pagerank"
            return 0.0

    def get_idf(self, w):
        try:
            q = "select idf.idf from idf where idf.word = \'%s\'" % w
            self.curr.execute(q)
            data = self.curr.fetchall()
            return float(data[0][0])
        except Exception as e:
            print e.__str__() + " idf"
            return 0.0

    def get_list(self, w):
        try:
            q = "select urls from index_word where index_word.word = \'%s\'" % w
            self.curr.execute(q)
            data = self.curr.fetchall()
            return eval('list(' + data[0][0] + ')')
        except Exception as e:
            print e.__str__() + " inverted"
            return None

    def get_tfidf(self, page):
        try:
            q = "select tfidf.tfidf from tfidf where tfidf.url in %s" % page.__str__()
            print q
            self.curr.execute(q)
            data = self.curr.fetchall()

            return data
            #return eval('dict(' + data[0][0] + ')')
        except Exception as e:
            print e.__str__() + " tf-idf " + page
            return {}

    def isStopWord(self, zbor):
        try:
            q = "select * from stopwords where stopwords.word = \'%s\'" % zbor
            self.curr.execute(q)
            data = self.curr.fetchall()
            if data:
                return True
            return False
        except Exception as e:
            print e.__str__() + " stopwords"
            return False

    def get_tf_idf(self):
        try:
            q = "select * from tfidf;"
            self.curr.execute(q)
            data = self.curr.fetchall()
            recnik = {}
            for dat in data:
                recnik[dat[0]] = eval(dat[1])
            return recnik
        except Exception as e:
            print e.__str__() + ' from get_tf_idf()'
            return None
"""
d = Database()
print d.isStopWord(raw_input('test stop_words:'))
print d.get_tfidf(raw_input('test get_tfidf:'))
print d.get_list(raw_input('test get_list:'))
print d.get_idf(raw_input('test get_idf:'))
print d.get_page_rank(raw_input('test get_page_rank:'))
"""