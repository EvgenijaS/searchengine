import numpy as np
import re
import operator
import math
from database import Database


def nash_f(string_term, number_n):
    data = search(string_term.encode('utf-8'))
    return data


# -------------------------------------------------------------------------------

c = 0.3  # rank relevance tradeoff

# -------------------------------------------------------------------------------


def search(query):
    """
       Return list relevant documents given the query as string
    """
    tokens = re.findall(ur"(?u)\w+", query.lower().decode('utf-8'))    # split on non-alpha character

    d = Database()

    # remove stop words
    tokens_copy = tokens[:]
    for word in tokens_copy:
        if d.isStopWord(word):
            tokens.remove(word)
    print 'stop zborovi otstraneti'

    # get relevant documents
    urls = []
    query_stems = []
    word_counter = {}

    for t in tokens:
        term = t
        res = d.get_list(term)

        while term and res is None:
            term = term[:-1]
            res = d.get_list(term)
        print res

        if res is not None:
            urls.extend(res)
            query_stems.append(term)

        for url_elem in res:
            word_counter.setdefault(url_elem, 0)
            word_counter[url_elem] += 1

    print 'dokumenti najdeni'
    print len(urls)

    # get document vectors (tf-idf representation)

    if tf_idf is None:
        print 'None tf_idf()'
        return []

    documents_tf_idf = []
    urls_tmp = []
    for url in urls:
        try:
            documents_tf_idf.append(tf_idf[url])
            urls_tmp.append(url)
        except:
            continue

    urls = urls_tmp

    # count common words between the query and the document
    words_common = []
    for url in urls:
        words_common.append(word_counter[url])

    print 'tfidf najdeni'

    # query to tf-idf vector
    stems_tfidf = term_freq(query_stems)                    # tf of the stems (dictionary)
    for stem in query_stems:
        stems_tfidf[stem] *= d.get_idf(stem)

    print 'query vo tfidf'

    # calculate the score for each document
    score = [(1 - c)*cosine_similarity(stems_tfidf, doc)*words_common[idx] for idx, doc in enumerate(documents_tf_idf)]

    print score
    print "score"

    result = dict(zip(urls, score))
    sortirani = sorted(result.items(), key=operator.itemgetter(1), reverse=True)[:50]
    urls = [a[0] for a in sortirani]
    score = [a[1] for a in sortirani]

    # get pageranks
    pageranks = [d.get_page_rank(url) for url in urls]

    # get score
    score = score + np.multiply(pageranks, c)

    print 'score presmetan'

    # sort documents by score
    result = dict(zip(urls, score))

    print 'rezultati sortirani'

    return sorted(result.items(), key=operator.itemgetter(1), reverse=True)


def term_freq(document):
    """
    Calculates term frequency of every word in a document
    document - list of words
    returns dictionary: term - term frequency
    """
    tf = {}
    for word in document:
        count = tf.get(word, 0)
        tf[word] = count + 1

    words_count = 0
    for key in tf:
        words_count += tf[key]

    for key in tf:
        tf[key] /= float(words_count)

    return tf
# -------------------------------------------------------------------------------


def cosine_similarity(query, doc):
    """ Both query and doc are dictionaries stem - tfidf """
    cos = 0

    for stem in query:
        if stem in doc:
            cos += (query[stem] * doc[stem])

    m1 = vector_magnitude(query)
    m2 = vector_magnitude(doc)

    cos /= float(m1*m2)
    return cos


def vector_magnitude(v):
    """ v is considered to be dictionary stem-tfidf"""
    m = 0
    for stem in v:
        m += v[stem]**2
    return math.sqrt(m)


###############################################################################
database = Database()

tf_idf = database.get_tf_idf()


###############################################################################
# -----------------TEST----------------------------------------------------------

def main():
    query = raw_input('Prebaraj: ')
    res = search(query)

    print res


if __name__ == '__main__':
    main()
